import './App.css';
import React from 'react';
import FormField from './component/Form';

function App() {
  return (
    <div className="app">
      <FormField />
    </div>
  );
}

export default App;
