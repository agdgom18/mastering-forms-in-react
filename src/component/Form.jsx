/* eslint-disable jsx-a11y/accessible-emoji */
import React, { useState } from 'react';
import Styles from './Styles';
import { Form, Field } from 'react-final-form';
import { onlyLettersLatin, required, maxLength, isNumber, maxAge } from './validationFunctions';

const style = {};

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const onSubmit = async (values) => {
  await sleep(300);
  window.alert(JSON.stringify(values, 0, 2));
};

const FormField = () => {
  const [inputValue, setInputValue] = useState({
    firstName: '',
    lastName: '',
    age: '',
  });

  const handleInputChange = (e) => {
    const value = e.target.value;
    const onlyLetters = /^[a-zA-Z]*$/.test(value);
    if (value.length <= 100 && value !== '' && onlyLetters) {
      e.target.name === 'firstName' ? setInputValue({ ...inputValue, firstName: value }) : setInputValue({ ...inputValue, lastName: value });
    }
  };

  const handleAgeInput = (e) => {
    const value = e.target.value;

    // Проверка на числовые символы
    const regex = /^[0-9]+$/.test(value);

    if (value.length <= 10 && value !== '' && regex) {
      setInputValue({ ...inputValue, age: value });
    }
  };

  const composeValidators =
    (...validators) =>
    (value) =>
      validators.reduce((error, validator) => error || validator(value), undefined);

  return (
    <Styles>
      <h1>React Final Form - Simple Example</h1>
      <a href="https://final-form.org/react" target="_blank" rel="noopener noreferrer">
        Read Docs
      </a>
      <Form
        onSubmit={onSubmit}
        validate={(values) => {
          const errors = {};
          if (!values.firstName) {
            errors.firstName = 'First name is required';
          }
          if (!values.lastName) {
            errors.lastName = 'Last name is required';
          }
          if (isNaN(values.age) || +values.age > 100) {
            errors.age = 'Age must be a number less than or equal to 100';
          }
          return errors;
        }}
        initialValues={{ stooge: 'larry', employed: false }}
        render={({ handleSubmit, form, submitting, pristine, values, invalid }) => (
          <form onSubmit={handleSubmit}>
            <div>
              <label>First Name</label>
              <Field
                initialValue={inputValue.firstName}
                onChange={(e) => handleInputChange(e)}
                name="firstName"
                validate={composeValidators(required, maxLength(100), onlyLettersLatin)}
                render={({ input, meta }) => (
                  <input style={{ border: meta.error && meta.touched ? '1px solid red' : '1px solid #ccc' }} {...input} placeholder="First Name" />
                )}
              />
            </div>
            <div>
              <label>Last Name</label>
              <Field
                name="lastName"
                initialValue={inputValue.lastName}
                onChange={(e) => handleInputChange(e)}
                type="text"
                placeholder="Last Name"
                validate={composeValidators(required, maxLength(100), onlyLettersLatin)}
                render={({ input, meta }) => (
                  <input style={{ border: meta.error && meta.touched ? '1px solid red' : '1px solid #ccc' }} {...input} placeholder="Last Name" />
                )}
              />
            </div>
            <div>
              <label>Age</label>
              <Field
                name="age"
                initialValue={inputValue.age}
                onChange={(e) => handleAgeInput(e)}
                type="text"
                validate={composeValidators(isNumber, maxAge)}
                render={({ input, meta }) => <input style={{ border: meta.error && meta.touched ? '1px solid red' : '1px solid #ccc' }} {...input} />}
              />
            </div>

            <div>
              <label>Employed</label>
              <Field name="employed" component="input" type="checkbox" />
            </div>
            <div>
              <label>Favorite Color</label>
              <Field name="favoriteColor" component="select">
                <option />
                <option value="#ff0000">❤️ Red</option>
                <option value="#00ff00">💚 Green</option>
                <option value="#0000ff">💙 Blue</option>
              </Field>
            </div>
            <div>
              <label>Toppings</label>
              <Field name="toppings" component="select" multiple>
                <option value="chicken">🐓 Chicken</option>
                <option value="ham">🐷 Ham</option>
                <option value="mushrooms">🍄 Mushrooms</option>
                <option value="cheese">🧀 Cheese</option>
                <option value="tuna">🐟 Tuna</option>
                <option value="pineapple">🍍 Pineapple</option>
              </Field>
            </div>
            <div>
              <label>Sauces</label>
              <div>
                <label>
                  <Field name="sauces" component="input" type="checkbox" value="ketchup" /> Ketchup
                </label>
                <label>
                  <Field name="sauces" component="input" type="checkbox" value="mustard" /> Mustard
                </label>
                <label>
                  <Field name="sauces" component="input" type="checkbox" value="mayonnaise" /> Mayonnaise
                </label>
                <label>
                  <Field name="sauces" component="input" type="checkbox" value="guacamole" /> Guacamole 🥑
                </label>
              </div>
            </div>
            <div>
              <label>Best Stooge</label>
              <div>
                <label>
                  <Field name="stooge" component="input" type="radio" value="larry" /> Larry
                </label>
                <label>
                  <Field name="stooge" component="input" type="radio" value="moe" /> Moe
                </label>
                <label>
                  <Field name="stooge" component="input" type="radio" value="curly" /> Curly
                </label>
              </div>
            </div>
            <div>
              <label>Notes</label>
              <Field name="notes" component="textarea" placeholder="Notes" />
            </div>
            <div className="buttons">
              <button type="submit" disabled={submitting || pristine || invalid}>
                Submit
              </button>
              <button type="button" onClick={form.reset} disabled={submitting || pristine}>
                Reset
              </button>
            </div>
            <pre>{JSON.stringify(values, 0, 2)}</pre>
          </form>
        )}
      />
    </Styles>
  );
};

export default FormField;
