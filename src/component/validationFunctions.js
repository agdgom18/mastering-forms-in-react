// validationFunctions.js
const required = (value) => (value ? undefined : 'Field is required');
const maxLength = (max) => (value) => value && value.length > max ? `Must be ${max} characters or less` : undefined;
const isNumber = (value) => (isNaN(value) ? 'Must be a number' : undefined);
const maxAge = (value) => (value > 100 ? 'Age must be 100 or less' : undefined);

const onlyLettersLatin = (value) => {
  const regex = /^[a-zA-Z]+$/;
  return regex.test(value) ? undefined : 'Only letters are allowed';
};

export { onlyLettersLatin, required, maxLength, isNumber, maxAge };
